#include "nwnx_sql"
#include "gui_portraits_conf"


//==============================================================================
// Implementation below

void RequestDynamicContent(object PCObject, string ResSha256Hash, string CallbackScript, string CallbackContext, string ResFileName, string DownloadUrl)
{
    SendMessageToPC(PCObject, "SCliExt17" + ResSha256Hash + CallbackScript + "|" + CallbackContext + "|" + ResFileName + "|" + DownloadUrl + "|");
}

void AddPortraitsRow(object oPC, string s1="", string s2="", string s3="", string s4="", int bUploaded=FALSE){

	string sTextures = "";
	string sText = "";
	string sHidden = "";
	if(s1 != ""){
		sText     += "LCDA_CUSTOM_PORTRAIT_RESREF_1="+s1+".tga;";
		sTextures += "LCDA_CUSTOM_PORTRAIT_ICON_1="+s1+".tga;";
		sHidden   += "LCDA_CUSTOM_PORTRAIT_1=unhide;LCDA_CUSTOM_PORTRAIT_RESREF_1=unhide;LCDA_CUSTOM_PORTRAIT_ICON_1=unhide;";
	}

	if(s2 != ""){
		sText     += "LCDA_CUSTOM_PORTRAIT_RESREF_2="+s2+".tga;";
		sTextures += "LCDA_CUSTOM_PORTRAIT_ICON_2="+s2+".tga;";
		sHidden   += "LCDA_CUSTOM_PORTRAIT_2=unhide;LCDA_CUSTOM_PORTRAIT_RESREF_2=unhide;LCDA_CUSTOM_PORTRAIT_ICON_2=unhide;";
	}

	if(s3 != ""){
		sText     += "LCDA_CUSTOM_PORTRAIT_RESREF_3="+s3+".tga;";
		sTextures += "LCDA_CUSTOM_PORTRAIT_ICON_3="+s3+".tga;";
		sHidden   += "LCDA_CUSTOM_PORTRAIT_3=unhide;LCDA_CUSTOM_PORTRAIT_RESREF_3=unhide;LCDA_CUSTOM_PORTRAIT_ICON_3=unhide;";
	}

	if(s4 != ""){
		sText     += "LCDA_CUSTOM_PORTRAIT_RESREF_4="+s4+".tga;";
		sTextures += "LCDA_CUSTOM_PORTRAIT_ICON_4="+s4+".tga;";
		sHidden   += "LCDA_CUSTOM_PORTRAIT_4=unhide;LCDA_CUSTOM_PORTRAIT_RESREF_4=unhide;LCDA_CUSTOM_PORTRAIT_ICON_4=unhide;";
	}

	sHidden += "CUSTOM_PORTRAIT_1=hide;CUSTOM_PORTRAIT_ICON_1=hide;";
	sHidden += "CUSTOM_PORTRAIT_2=hide;CUSTOM_PORTRAIT_ICON_2=hide;";
	sHidden += "CUSTOM_PORTRAIT_3=hide;CUSTOM_PORTRAIT_ICON_3=hide;";
	sHidden += "CUSTOM_PORTRAIT_4=hide;CUSTOM_PORTRAIT_ICON_4=hide;";

	sText += "CUSTOM_PORTRAIT_UPLOADED=" + IntToString(bUploaded);

	AddListBoxRow(oPC, "SCREEN_PORTRAIT_SELECT", "PORTRAIT_LISTBOX",
			"", sText, sTextures, "", sHidden);

}

void AddHeaderRow(object oPC, string sTitle, string sIcon=""){
	string sHidden;
	sHidden += "CUSTOM_PORTRAIT_1=hide;CUSTOM_PORTRAIT_ICON_1=hide;";
	sHidden += "CUSTOM_PORTRAIT_2=hide;CUSTOM_PORTRAIT_ICON_2=hide;";
	sHidden += "CUSTOM_PORTRAIT_3=hide;CUSTOM_PORTRAIT_ICON_3=hide;";
	sHidden += "CUSTOM_PORTRAIT_4=hide;CUSTOM_PORTRAIT_ICON_4=hide;";
	sHidden += "LCDA_CUSTOM_PORTRAIT_1=hide;LCDA_CUSTOM_PORTRAIT_ICON_1=hide;";
	sHidden += "LCDA_CUSTOM_PORTRAIT_2=hide;LCDA_CUSTOM_PORTRAIT_ICON_2=hide;";
	sHidden += "LCDA_CUSTOM_PORTRAIT_3=hide;LCDA_CUSTOM_PORTRAIT_ICON_3=hide;";
	sHidden += "LCDA_CUSTOM_PORTRAIT_4=hide;LCDA_CUSTOM_PORTRAIT_ICON_4=hide;";
	sHidden += "SEPARATOR=unhide;";
	if(sIcon=="")
		sHidden += "SEPARATOR_ICON=hide;";
	AddListBoxRow(oPC, "SCREEN_PORTRAIT_SELECT", "PORTRAIT_LISTBOX",
			"",//name
			"SEPARATOR_TEXT="+sTitle,//text
			"SEPARATOR_ICON="+sIcon,//icons
			"",//vars
			sHidden);//hide/unhide
}


string RandomBase64(int nLength){
	string s;
	int i;
	for(i = 0 ; i < nLength ; i++){
		switch(Random(64)){
			case  0: s += "A"; break; case 17: s += "R"; break; case 34: s += "i"; break; case 51: s += "z"; break;
			case  1: s += "B"; break; case 18: s += "S"; break; case 35: s += "j"; break; case 52: s += "0"; break;
			case  2: s += "C"; break; case 19: s += "T"; break; case 36: s += "k"; break; case 53: s += "1"; break;
			case  3: s += "D"; break; case 20: s += "U"; break; case 37: s += "l"; break; case 54: s += "2"; break;
			case  4: s += "E"; break; case 21: s += "V"; break; case 38: s += "m"; break; case 55: s += "3"; break;
			case  5: s += "F"; break; case 22: s += "W"; break; case 39: s += "n"; break; case 56: s += "4"; break;
			case  6: s += "G"; break; case 23: s += "X"; break; case 40: s += "o"; break; case 57: s += "5"; break;
			case  7: s += "H"; break; case 24: s += "Y"; break; case 41: s += "p"; break; case 58: s += "6"; break;
			case  8: s += "I"; break; case 25: s += "Z"; break; case 42: s += "q"; break; case 59: s += "7"; break;
			case  9: s += "J"; break; case 26: s += "a"; break; case 43: s += "r"; break; case 60: s += "8"; break;
			case 10: s += "K"; break; case 27: s += "b"; break; case 44: s += "s"; break; case 61: s += "9"; break;
			case 11: s += "L"; break; case 28: s += "c"; break; case 45: s += "t"; break; case 62: s += "+"; break;
			case 12: s += "M"; break; case 29: s += "d"; break; case 46: s += "u"; break; case 63: s += "/"; break;
			case 13: s += "N"; break; case 30: s += "e"; break; case 47: s += "v"; break;
			case 14: s += "O"; break; case 31: s += "f"; break; case 48: s += "w"; break;
			case 15: s += "P"; break; case 32: s += "g"; break; case 49: s += "x"; break;
			case 16: s += "Q"; break; case 33: s += "h"; break; case 50: s += "y"; break;
		}
	}
	return s;
}

string UrlEncode(string s){
	string res;
	int nLen = GetStringLength(s);
	int i;
	for(i = 0 ; i < nLen ; i++){
		string sChar = GetSubString(s, i, 1);
		int nChar = CharToASCII(sChar);
		if((nChar >= 0x30 && nChar <= 0x39) // Numbers
		|| (nChar >= 0x41 && nChar <= 0x5A) // Capital letters
		|| (nChar >= 0x61 && nChar <= 0x7A) // Small letters
		|| (nChar == 0x2D || nChar == 0x5F || nChar == 0x2E || nChar == 0x7E) // -_.~
		){
			res += sChar;
		}
		else{
			string sHex = IntToHexString(nChar);
			if(nChar > 0xFF)
				res += "%" + GetSubString(sHex, 6, 2);
			res += "%" + GetSubString(sHex, 8, 2);
		}
	}
	return res;
}

void CacheCharacterPortraitInfo(object oPC){
	string sFileName = GetUploadedPortrait(oPC);
	if(sFileName != ""){
		SQLExecDirect("SELECT `sha256` FROM `portraits_upload_list` WHERE `account_name`='"+SQLEncodeSpecialChars(GetPCPlayerName(oPC))+"' AND `file_name`='"+SQLEncodeSpecialChars(sFileName)+"'");
		if(SQLFetch()){
			SetLocalString(oPC, "uploaded_portrait_name", sFileName);
			SetLocalString(oPC, "uploaded_portrait_url", PORTRAITS_BASE_URL + UrlEncode(GetPCPlayerName(oPC)) + "/" + UrlEncode(sFileName));
			SetLocalString(oPC, "uploaded_portrait_sha", SQLGetData(1));
		}
	}
	else{
		DeleteLocalString(oPC, "uploaded_portrait_name");
	}
}

void DownloadPortraitForModule(string sFileName, string sUrl, string sSha){
	object oPC = GetFirstPC();
	while(GetIsObjectValid(oPC)){
		if(GetLocalInt(oPC, "__cliext_version__") >= 3)
			RequestDynamicContent(oPC, sSha, "", "", sFileName, sUrl);
		oPC = GetNextPC();
	}
	RequestDynamicContent(oPC, sSha, "", "", sFileName, sUrl);
}

void DownloadModulePortraitsForPC(object oPC){
	object oPlayer = GetFirstPC();
	while(GetIsObjectValid(oPlayer)){
		if(oPC != oPlayer
		&& GetLocalInt(oPlayer, "__cliext_version__") >= 3
		&& GetLocalString(oPlayer, "uploaded_portrait_name") != ""){
			RequestDynamicContent(
				oPC,
				GetLocalString(oPlayer, "uploaded_portrait_sha"),
				"",
				"",
				GetLocalString(oPlayer, "uploaded_portrait_name"),
				GetLocalString(oPlayer, "uploaded_portrait_url")
			);
		}
		oPlayer = GetNextPC();
	}
}

void main(string cmd, string s1, string s2)
{
	object oPC = OBJECT_SELF;

	if(cmd == "onclientextenter"){
		if(GetLocalInt(oPC, "__cliext_version__") < 3)
			return;

		CacheCharacterPortraitInfo(oPC);

		if(GetLocalString(oPC, "uploaded_portrait_name") != ""){
			// Make entering PC's portrait available to everybody
			DownloadPortraitForModule(
				GetLocalString(oPC, "uploaded_portrait_name"),
				GetLocalString(oPC, "uploaded_portrait_url"),
				GetLocalString(oPC, "uploaded_portrait_sha")
			);

			// Download portraits that are currently used by players in the module
			DownloadModulePortraitsForPC(oPC);
		}
	}
	else if(cmd=="preload"){
		if(s1 == "clear")
			ClearListBox(oPC, "SCREEN_PORTRAIT_SELECT", "PORTRAIT_LISTBOX");

		if(GetLocalInt(oPC, "__cliext_version__") >= 3){
			string sAccount = GetPCPlayerName(oPC);

			int bFoundPortraits = FALSE;

			SQLExecDirect("SELECT `file_name`, `sha256` FROM `portraits_upload_list` WHERE `account_name`='"+SQLEncodeSpecialChars(sAccount)+"'");
			while(SQLFetch()){
				bFoundPortraits = TRUE;
				string sFileName = SQLGetData(1);
				string sSha = SQLGetData(2);
				string sUrl = PORTRAITS_BASE_URL + UrlEncode(sAccount) + "/" + UrlEncode(sFileName);

				SetLocalInt(oPC, "portraits_preload_remaining", GetLocalInt(oPC, "portraits_preload_remaining") + 1);
				RequestDynamicContent(oPC, sSha, "gui_portraits_ondl", "", sFileName, sUrl);
			}

			if(!bFoundPortraits)
				main("populate", "", "");
		}
		else{
			main("populate", "", "");
			SetGUIObjectDisabled(oPC, "SCREEN_PORTRAIT_SELECT", "UPLOAD_PORTRAIT", TRUE);
			SetGUIObjectDisabled(oPC, "SCREEN_PORTRAIT_SELECT", "UPLOAD_PORTRAIT_REF", TRUE);
		}
	}
	if(cmd=="populate"){
		if(GetLocalInt(oPC, "__cliext_version__") >= 3){
			string sAccount = GetPCPlayerName(oPC);

			int bFirst = TRUE;
			int nCount = 0;
			string s1, s2, s3;

			SQLExecDirect("SELECT `file_name` FROM `portraits_upload_list` WHERE `account_name`='"+SQLEncodeSpecialChars(sAccount)+"' ORDER BY `file_name` ASC");
			while(SQLFetch()){
				if(bFirst){
					bFirst = FALSE;
					AddHeaderRow(oPC, LOC_YOURPORTRAITS, "ico_generic_64.tga");
				}

				string sPortraitName = SQLGetData(1);
				sPortraitName = GetStringLeft(sPortraitName, GetStringLength(sPortraitName) - 4); // Remove .tga

				// Insert portraits in rows of 4
				switch(nCount++ % 4){
					case 0: s1 = sPortraitName; break;
					case 1: s2 = sPortraitName; break;
					case 2: s3 = sPortraitName; break;
					case 3:
						AddPortraitsRow(oPC, s1, s2, s3, sPortraitName, TRUE);
						s1 = ""; s2 = "";  s3 = "";
				}
			}

			// Insert remaining portraits if any
			if(s1 != "")
				AddPortraitsRow(oPC, s1, s2, s3, "", TRUE);
		}

		AddModulePortraits(oPC);
	}
	else if(cmd=="select"){
		SetLocalGUIVariable(oPC, "SCREEN_PORTRAIT_SELECT", 0, s1);
		SetLocalGUIVariable(oPC, "SCREEN_PORTRAIT_SELECT", 11, s2);
	}
	else if(cmd=="apply"){
		string sFileName = s1;
		int bUploaded = StringToInt(s2);

		SaveUploadedPortraitName(oPC, bUploaded ? sFileName : "");

		CacheCharacterPortraitInfo(oPC);
		DownloadModulePortraitsForPC(oPC);
	}
	else if(cmd=="open_url"){
		string sAccount = GetPCPlayerName(oPC);
		string sToken = RandomBase64(PORTRAITS_TOKEN_LENGTH);

		SQLExecDirect("DELETE FROM `portraits_upload_tokens` WHERE `account_name`='" + SQLEncodeSpecialChars(sAccount) + "' AND `expire_ts` < NOW()");

		SQLExecDirect("INSERT INTO `portraits_upload_tokens` (`account_name`,`token`,`expire_ts`) VALUES ('"+SQLEncodeSpecialChars(sAccount)+"','"+sToken+"',DATE_ADD(NOW(), INTERVAL " + IntToString(PORTRAITS_TOKEN_DURATION_MINUTES) + " MINUTE))");
		string sUrl = PORTRAITS_UPLOAD_URL + "?account=" + UrlEncode(sAccount) + "&token=" + UrlEncode(sToken);
		if(GetStringLeft(sUrl, 8) == "https://")
			sUrl = GetSubString(sUrl, 8, -1);
		if(GetStringLeft(sUrl, 7) == "http://")
			sUrl = GetSubString(sUrl, 7, -1);

		DisplayGuiScreen(oPC, "SCREEN_PORTRAIT_SELECT_URLOPEN", FALSE, "portrait_change_urlopen.xml");
		SetLocalGUIVariable(oPC, "SCREEN_PORTRAIT_SELECT_URLOPEN", 10, sUrl);
		CloseGUIScreen(oPC, "SCREEN_PORTRAIT_SELECT_URLOPEN");
	}
	else if(cmd=="onmoduleload"){
		SQLExecDirect("CREATE TABLE IF NOT EXISTS `portraits_upload_tokens` ("
			+ "`account_name` VARCHAR(45) NOT NULL,"
			+ "`token` VARCHAR(" + IntToString(PORTRAITS_TOKEN_LENGTH) + ") NOT NULL,"
			+ "`expire_ts` TIMESTAMP NULL,"
			+ "PRIMARY KEY (`account_name`, `token`)) DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_bin"
		);
		SQLExecDirect("DELETE FROM `portraits_upload_tokens` WHERE `expire_ts` < NOW()");

		SQLExecDirect("CREATE TABLE IF NOT EXISTS `portraits_upload_list` ("
			+ "`account_name` VARCHAR(45) NOT NULL,"
			+ "`file_name` VARCHAR(36) NOT NULL,"
			+ "`sha256` VARCHAR(64) NOT NULL,"
			+ "PRIMARY KEY (`account_name`, `file_name`)) DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_bin"
		);
	}

}
