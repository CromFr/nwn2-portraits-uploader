void AddHeaderRow(object oPC, string sTitle, string sIcon="");
void AddPortraitsRow(object oPC, string s1="", string s2="", string s3="", string s4="", int bUploaded=FALSE);
//==============================================================================
// Configuration

// URL to open for the upload portrait web page
const string PORTRAITS_UPLOAD_URL = "https://lcda-nwn2.fr/portraits_upload.php";

// URL where portraits are stored to be downloaded
const string PORTRAITS_BASE_URL = "https://lcda-nwn2.fr/portraits/";

// Authentication tokens are generated when a player clicks to open the upload page
// Tokens are valid for PORTRAITS_TOKEN_DURATION_MINUTES minutes
const int PORTRAITS_TOKEN_DURATION_MINUTES = 15;
// Generated token base64 characters length
const int PORTRAITS_TOKEN_LENGTH = 16;

// Implement this to keep track of an uploaded portraits that has been set to
// a specific PC. This function is called only when a player selects a
// portrait that requires a specific file to be downloaded with the client
// extension.
//
// The portrait info can be stored in a PC's item local variable
// (recommended), or a persistent database like MySQL or Campaign DB (can
// cause minor issues if the server crashes before the PC is exported).
void SaveUploadedPortraitName(object oPC, string sUploadedPortrait){
	if(sUploadedPortrait != "")
		SetCampaignString("custom_portraits", "portrait", sUploadedPortrait, oPC);
	else
		DeleteCampaignVariable("custom_portraits", "portrait", oPC);
}

// Implement this to keep track of an uploaded portraits that has been set to
// a specific PC.
//
// This function must return "" if the player doesn't use an uploaded
// portrait. Otherwise it must return the portrait file name
string GetUploadedPortrait(object oPC){
	return GetCampaignString("custom_portraits", "portrait", oPC);
}

// This function is called to add custom portraits that are stored into HAK or
// base game files
//
// You can leave the function empty if you don't want to provide a base set of
// portraits to all your players.
void AddModulePortraits(object oPC){
	AddHeaderRow(oPC, "Neverwinter Nights 2", "ico_generic_64.tga");
	AddPortraitsRow(oPC, "ico_umoja",     "ico_soraevora", "ico_Septimund",       "ico_Quarrel");
	AddPortraitsRow(oPC, "ico_Lastri1",   "ico_Inshula",   "ico_Grykk",           "ico_finch");
	AddPortraitsRow(oPC, "ico_Chir",      "ico_Belueth",   "ico_ribsmasher",      "");
	AddPortraitsRow(oPC, "ico_dove",      "ico_gann",      "ico_safiya",          "");
}



//==============================================================================
// Localization

const string LOC_YOURPORTRAITS = "Your portraits";