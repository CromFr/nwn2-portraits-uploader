void main(string sScriptContext, string sUrl, int bSuccess, string sError){

	object oPC = OBJECT_SELF;

	if(!bSuccess)
		SendMessageToPC(oPC, "Failed to download portrait " + sUrl + ": " + sError);
	else if(sError == "downloaded")
		SendMessageToPC(oPC, "Downloaded portrait: " + sUrl);

	int nRem = GetLocalInt(oPC, "portraits_preload_remaining") - 1;
	SetLocalInt(oPC, "portraits_preload_remaining", nRem);
	if(nRem == 0){
		ClearScriptParams();
		AddScriptParameterString("populate");
		AddScriptParameterString("");
		AddScriptParameterString("");
		ExecuteScriptEnhanced("gui_portraits", oPC);
	}
}