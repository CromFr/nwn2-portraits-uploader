# Portraits upload system

These scripts and files lets players of NWN2 server upload their own custom
character portraits. It contains a customized portraits UI with some NWN2
scripts, and a web page for uploading and managing custom portraits.

It leverages the Dynamic Content feature of Skywing's Client Extension to
automatically download the required portraits in the background.


## Usage
The portraits selection UI can be accessed by double-clicking on the
character's portrait / head view in the character sheet panel.

Clicking the upload button generates a random and temporary access token, and opens the
upload page in the browser.

On the upload page, players can send their portrait image files. The portraits
are automatically resized if needed, and converted to the appropriate format
for NWN2 (TGA).

Once portraits are uploaded, players can return to NWN2, click the refresh
button and select their new portraits. New portraits are automatically
downloaded by other players on the server.

# Requirements

For the server, you need:
- a NWN2 server
- a MariaDB or MySQL server
- a HTTP server with PHP

Players will need to use Skywing's Client Extension in order to view the
portraits in-game.

# Install instructions

## HTTP server

Your server must:
- Listen to HTTP connections, and ideally redirect them to HTTPS (NWN2 can
  only open HTTP links in the browser).
- Have read + write access to the portraits upload directory
- Interpret `.php` files as PHP code
- Have the following PHP extensions installed and enabled:
  [`imagick`](https://www.php.net/manual/en/book.imagick.php)

Installation:
1. Copy `portraits_upload.php` and `tga.js` on your web server
2. Edit the top part of `portraits_upload.php`, specifically
    - `$mysql_XXX` variables to setup the MySQL database access
    - `$upload_file_path`: The path to the folder where portraits will be
      stored on the webserver after upload. This folder must be write-able by
      the PHP interpreter.
    - `$portraits_base_url`: Public URL where the `$upload_file_path` folder
      is served by the HTTP server.
    - `$portrait_size`: Required size in pixels for the uploaded portraits
      (they will be resized to this size if they don't match).
3. Opening the upload page on the browser should result in a message _Missing
   account and/or token url parameters_. If not, there is probably a
   configuration issue on your php server or `portraits_upload.php`. Don't
   worry about the account/token message, the missing information will be
   provided by the game when clicking the in-game upload button.

## NWScript

### Module OnClientEnter
Insert this code somewhere in the `main` function of the script that is
triggered when a player enters the server.

```cpp
// Reset cliext version variable.
// This will be set again by gui_scliext_identify later if the cliext is installed
SetLocalInt(GetEnteringObject(), "__cliext_version__", 0);
```

### Module OnModuleLoad
Insert this code somewhere in the `main` function of the script that is
triggered when the module has finished loading.

```cpp
// Init uploaded portraits SQL tables
ClearScriptParams();
AddScriptParameterString("onmoduleload");
AddScriptParameterString("");
AddScriptParameterString("");
ExecuteScriptEnhanced("gui_portraits", OBJECT_SELF);
```

### `gui_scliext_identify.nss`
You probably already have `gui_scliext_identify.nss` in your module. If not,
copy the file from this repository into your module.

If you already have `gui_scliext_identify.nss` in your module, copy this code
somewhere in the `main` function:
```cpp
// Store cliext version on PC (this is reset every time a client enters the module)
SetLocalInt(OBJECT_SELF, "__cliext_version__", nFlagsAndProtocolVersion);

// Load custom portraits if needed
ClearScriptParams();
AddScriptParameterString("onclientextenter");
AddScriptParameterString("");
AddScriptParameterString("");
ExecuteScriptEnhanced("gui_portraits", OBJECT_SELF);
```

### `gui_portraits*`

Copy all `gui_portraits*.nss` files into your module. Open the
`gui_portraits_conf.nss` file and edit the configuration to match
`portraits_upload.php`'s config.

- `PORTRAITS_BASE_URL` must match `$portraits_base_url` from `portraits_upload.php`
- `PORTRAITS_UPLOAD_URL` is the public URL where `portraits_upload.php` is
  served.

##### Portraits saving

There is currently no ways to retrieve the portrait that has been set to a
player character using NWScript alone. To overcome this limitation, this
script stores the portraits in a database when it is selected by the player.
By default `SaveUploadedPortraitName`, and `GetUploadedPortrait` use a local
variable on an item tagged `journalNODROP` that is given to all players that
enters _La Colère d'Aurile_ (my PW server) and that cannot be lost.

**You must change this** to an item's tag that will never quit the player
(cursed flag) on your module. 

Alternatively you can re-implement those two functions to instead use campaign
variables or the SQL database. It is recommended to store the portraits info
on a local variable on an item because it will be saved when the character is
exported, and prevents minor issues if the server crashes.

## GUI / XML
Copy `portrait_change.xml` and `portrait_change_urlopen.xml` inside your
module HAK files (or in the override folder for testing).

