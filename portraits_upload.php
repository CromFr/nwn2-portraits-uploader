<?php
// =============================================================================
// Configuration

// MySQL authentication info
$mysql_host = "localhost";
$mysql_user = "nwnx";
$mysql_password = "yourpassword";
$mysql_database = "nwnx";
$mysql_charset = "utf8";

// Path where uploaded image will be stored on disk
$upload_file_path = "/srv/http/portraits/"; // Make sure it ends with a /

// Public address where to download the portraits
$portraits_base_url = "https://lcda-nwn2.fr/portraits/"; // Make sure it ends with a /

// Required portrait image size in pixels
$portrait_size = [128, 128];

// Maximum number of uploaded portraits for each player account. Prevents from filling the disk
$portraits_upload_limit = 100;


// =============================================================================

// Implementation below

// Localization
$locale = $_SERVER['HTTP_ACCEPT_LANGUAGE'];
if(substr($locale, 0, 2) == "fr"){
	$loc = [ // French
		"pagetitle" => "Uploader un portrait",
		"imagesizeinfo" => "L'image sera retaillée et redimentionnée en %s px",
		"upload" => "Envoyer",
		"delete" => "Supprimer",

		"methodnotallowed" => "Méthode HTTP non autorisée",
		"missingauthinfo" => "Informations d'authentification (account, token) non trouvées",
		"failedauth" => "Unauthorized: Echec de l'authentification. Re-cliquez sur le bouton d'upload dans NWN2",
		"noimagereceived" => "L'image n'a pas été reçue (pas d'image ou image trop volumineuse)",
		"cannotreadimage" => "Impossible de lire le fichier d'image. Essayez un format différent",
		"cannotsaveimage" => "Impossible de sauvegarder le portrait",
		"cannotdeleteimage" => "L'image n'a pas pu être supprimée",
		"uploadlimitreached" => "Vous avez atteint la limite de " . $portraits_upload_limit . " portraits uploadés. Vous pouvez supprimer des portraits pour en uploader des nouveaux.",
	];
}
else{
	$loc = [ // English
		"pagetitle" => "Upload a portrait",
		"imagesizeinfo" => "The image will be cropped and resized to meet %s px",
		"upload" => "Upload",
		"delete" => "Delete",

		"methodnotallowed" => "Method not allowed",
		"missingauthinfo" => "Missing account and/or token url parameters",
		"failedauth" => "Unauthorized: Click again on the upload button in NWN2",
		"noimagereceived" => "No image has been received (no image sent or the image was too big)",
		"cannotreadimage" => "Could not read image. Try another image format.",
		"cannotsaveimage" => "Cannot save image",
		"cannotdeleteimage" => "Cannot delete image",
		"uploadlimitreached" => "You reached the limit of " . $portraits_upload_limit . " uploaded portraits. You can delete some portraits in order to upload new ones.",
	];
}


// Restrict HTTP methods
$method = $_SERVER["REQUEST_METHOD"];
if($method != "GET" && $method != "POST" && $method != "DELETE"){
	http_response_code(405);
	echo($loc["methodnotallowed"]);
	die();
}

// Get auth info
if(!isset($_REQUEST["account"]) || !isset($_REQUEST["token"])){
	http_response_code(400);
	echo($loc["missingauthinfo"]);
	die();
}
$account = $_REQUEST["account"];
$token = $_REQUEST["token"];

// Authenticate
$db = new mysqli($mysql_host, $mysql_user, $mysql_password, $mysql_database);
$db->set_charset($mysql_charset);

$auth_stmt = $db->prepare("SELECT COUNT(*) AS `count` FROM `portraits_upload_tokens` WHERE `account_name`=? AND `token`=? AND `expire_ts`>=NOW()");
$auth_stmt->bind_param("ss", $account, $token);
$auth_stmt->execute();
$count = $auth_stmt->get_result()->fetch_row()[0];

if($count != 1){
	http_response_code(401);
	echo($loc["failedauth"]);
	die();
}

function filenameencode($name) {
	// remove illegal file system characters https://en.wikipedia.org/wiki/Filename#Reserved_characters_and_words
	$name = str_replace(array_merge(
		array_map('chr', range(0, 31)),
		array('<', '>', ':', '"', '/', '\\', '|', '?', '*')
	), '', $name);
	// maximise filename length to 255 bytes http://serverfault.com/a/9548/44086
	$ext = pathinfo($name, PATHINFO_EXTENSION);
	$name= mb_strcut(pathinfo($name, PATHINFO_FILENAME), 0, 255 - ($ext ? strlen($ext) + 1 : 0), mb_detect_encoding($name)) . ($ext ? '.' . $ext : '');
	return $name;
}

function getprettyname($account_name, $file_path){
	$len = strlen($account_name);
	$file_path = basename($file_path, ".tga");
	if(substr($file_path, 0, $len + 3) == "p_" . filenameencode($account_name) . "_"){
		return substr($file_path, $len + 3);
	}
	return $file_path;
}


if($method == "GET"){

	$stmt = $db->prepare("SELECT `file_name` FROM `portraits_upload_list` WHERE `account_name`=?");
	$stmt->bind_param("s", $account);
	$stmt->execute();
	$res = $stmt->get_result();

	?>
	<html>
	<head>
		<title><?php echo($loc["pagetitle"]) ?></title>
		<script type="text/javascript" src="tga.js"></script>
		<style>
			body{
				text-align: center;
				background-color: #222;
			}
			#content{
				background-color: #eee;
				max-width: 850px;
				padding: 1em;
				margin: auto;
				border-radius: 0.5em;
			}
			#portraits-list{
				width: 100%;
				display: flex;
				flex-direction: row;
				flex-wrap: wrap;
				justify-content: center;
			}
			#portraits-list > .portrait {
				flex: 0 1 auto;
				align-self: auto;
				margin: 0.5em;
				background-color: #aaa;
				border-radius: 0.5em;
				padding: 0.5em;
				box-shadow: 3px 3px 5px #00000080;
			}
			#portraits-list > .portrait form {
				margin: 0;
			}

		</style>
	</head>
	<body>
		<div id="content">
			<div>
				<h3><?php echo($loc["pagetitle"]) ?></h3>
				<p>
					<?php echo(sprintf($loc["imagesizeinfo"], implode("x", $portrait_size))) ?>
				</p>
				<form action="<?php echo($_SERVER['REQUEST_URI']) ?>" method="post" enctype="multipart/form-data">
					<input type="file" id="myFile" name="portrait_file" accept="image/*,image/tga">
					<input type="submit" value="<?php echo($loc["upload"]) ?>">
					<input type="hidden" name="account" value="<?php echo($account) ?>">
					<input type="hidden" name="token" value="<?php echo($token) ?>">
				</form>
			</div>
			<div id="portraits-list">
				<?php
					while($data = $res->fetch_assoc()){
						$image_url = $portraits_base_url . $account . "/" . $data["file_name"];
						?>
							<div class="portrait" file_name="<?php echo($data["file_name"]) ?>">
								<div class="tga" image_url="<?php echo($image_url) ?>"></div>
								<div><a href="<?php echo($image_url) ?>"><?php echo(getprettyname($account, $data["file_name"])) ?></a></div>
								<button><?php echo($loc["delete"]) ?></button>
							</div>
						<?php
					}
				?>
			</div>
		</div>
		<script type="text/javascript">
			document.querySelectorAll("#portraits-list .portrait .tga").forEach(function(elmt){
				let tga = new TGA();
				tga.open( elmt.getAttribute("image_url"), function(data){
					var canvas = tga.getCanvas();
					elmt.appendChild(canvas);
				});
			});

			document.querySelectorAll("#portraits-list .portrait button").forEach(function(elmt){
				let file_name = elmt.parentElement.getAttribute("file_name");
				elmt.onclick = function(){
					let xhr = new XMLHttpRequest();
					xhr.open("DELETE", window.location.href + "&file_name=" + encodeURIComponent(file_name));
					xhr.onreadystatechange = function(){
						if (xhr.readyState === XMLHttpRequest.DONE) {
							window.location.reload();
						}
					};
					xhr.send();
				}
			});
		</script>
	</body>
	</html>
	<?php
}
elseif($method == "POST"){
	$src_file_name = basename($_FILES["portrait_file"]["name"]);
	$src_ext = pathinfo($src_file_name, PATHINFO_EXTENSION);
	$target_file_name = substr("p_" . $account . "_" . filenameencode(strtolower(pathinfo($src_file_name, PATHINFO_FILENAME))), 0, 32) . ".tga";
	$target_file = $upload_file_path . $account . "/" . $target_file_name;

	// Check upload count limit
	$stmt = $db->prepare("SELECT COUNT(*) FROM `portraits_upload_list` WHERE `account_name`=?");
	$stmt->bind_param("s", $account);
	$stmt->execute();
	$res = $stmt->get_result();
	if($res->fetch_array()[0] >= $portraits_upload_limit){
		http_response_code(400);
		echo($loc["uploadlimitreached"]);
		die();
	}

	// Check that a file has been received
	if($_FILES["portrait_file"]["tmp_name"] == ""){
		http_response_code(400);
		echo($loc["noimagereceived"]);
		die();
	}

	$im = new Imagick();

	// Force TGA format if there is a .tga extension
	if($src_ext == "tga")
		$im->setFormat("TGA");

	// Read image file
	try{
		$res = $im->readImage($_FILES["portrait_file"]["tmp_name"]);
		if(!$res)
			throw new Exception("res=" . str($res));
	}
	catch(ImagickException $e){
		http_response_code(400);
		echo($loc["cannotreadimage"]);
		error_log("Warning: " . $e);
		die();
	}

	# Set image orientation for better compatibility
	$im->setImageOrientation(imagick::ORIENTATION_BOTTOMLEFT);

	// Check image size
	$size = [$im->getImageWidth(), $im->getImageHeight()];
	if($size != $portrait_size){
		// Crop to meet target image ratio
		$targetRatio = $portrait_size[0] / $portrait_size[1];
		$imageRatio = $size[0] / $size[1];

		if($imageRatio >= $targetRatio){
			// height limit
			$cropSize = [$targetRatio * $size[1], $size[1]];
		}
		else{
			// width limit
			$cropSize = [$size[0], $size[0] / $targetRatio];
		}


		$im->cropImage($cropSize[0], $cropSize[1],
            ($size[0] - $cropSize[0]) / 2,
            ($size[1] - $cropSize[1]) / 2
        );

		// Resize image to the correct size
		$im->resizeImage($portrait_size[0], $portrait_size[1], imagick::FILTER_LANCZOS, 1);

		// http_response_code(400);
		// echo(sprintf($loc["badimagesize"], implode("x", $size), implode("x", $portrait_size)));
		// die();
	}

	// Create account dir for storing the image
	if(!is_dir($upload_file_path . $account)){
		mkdir($upload_file_path . $account, 0755);
	}

	// Write image file
	$im->setImageFormat("tga");
	$im->setFormat("tga");
	$im->setColorspace(imagick::COLORSPACE_RGB);
	if(!$im->writeImage($target_file)){
		http_response_code(500);
		echo($loc["cannotsaveimage"]);
		die();
	}

	// Store image info into MySQL
	$sha256 = hash_file("sha256", $target_file);
	$stmt = $db->prepare("INSERT INTO `portraits_upload_list` (`account_name`,`file_name`,`sha256`) VALUES(?,?,?) ON DUPLICATE KEY UPDATE sha256=?");
	$stmt->bind_param("ssss", $account, $target_file_name, $sha256, $sha256);
	$stmt->execute();

	echo("Success");
	header("Location: " . $_SERVER['REQUEST_URI']);
}
elseif($method == "DELETE"){

	$file_name = $_REQUEST["file_name"];
	$target_file = $upload_file_path . filenameencode($account) . "/" . $file_name;

	// Unregister file
	$stmt = $db->prepare("DELETE FROM `portraits_upload_list` WHERE `account_name`=? AND `file_name`=?");
	$stmt->bind_param("ss", $account, $file_name);
	$stmt->execute();
	$res = $stmt->get_result();

	// Remove file
	if(file_exists($target_file)){
		unlink($target_file);
	}

	if($db->affected_rows != 1){
		http_response_code(500);
		echo($loc["cannotdeleteimage"]);
		die();
	}

	// Return to list
	echo("Success");
	header("Location: " . $_SERVER['REQUEST_URI']);
}

?>
